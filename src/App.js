import React, { Component } from 'react';
import Person from "./Person/Person";
import './App.css';

class App extends Component {

  state = {
    persons: [
      {name: "Max", age: 29},
      {name: "Sergey", age: 29},
      {name: "Vika", age: 22}
    ]
  }

  switchName = (newName) => {
    this.setState({
      persons: [
        {name: newName, age: 30},
        {name: "Vuka", age: 25},
        {name: "Sabrina", age: 23},
      ]
    })
  }

  render() {
    return (
      <div className="App">
        <h1>Hi, I'm a React App!</h1>
        <button onClick={this.switchName.bind(this, "Kenguru")}>Switch Name</button>
        <Person name={this.state.persons[0].name} age={this.state.persons[0].age} />
        <Person name={this.state.persons[1].name} age={this.state.persons[1].age} click={this.switchName.bind(this, "Lolo")}>My Hobbies: Racing</Person>
        <Person name={this.state.persons[2].name} age={this.state.persons[2].age} />
      </div>
    );

    // return React.createElement("h1", {className: "heading"}, "Here goes the render!")
  }
}

export default App;
